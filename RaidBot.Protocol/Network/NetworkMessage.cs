﻿using RaidBot.Common.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaidBot.Protocol.Messages
{
    public abstract class NetworkMessage : MarshalByRefObject
    {
        public byte[] Data { get; set; }
  
        public abstract uint MessageId { get; }

        public abstract void Serialize(ICustomDataWriter writer);
        public abstract void Deserialize(ICustomDataReader reader);

        public void Pack(ICustomDataWriter writer)
        {
            Serialize(writer);//write all param of the packet
            byte[] data = writer.Data;//get the packet in array
            writer.Clear();//clear the writer for write header

            byte typeLen;//get the size of the size
            if (data.Length > 65535)
                typeLen = 3;
            else if (data.Length > 255)
                typeLen = 2;
            else if (data.Length > 0)
                typeLen = 1;
            else
                typeLen = 0;

            writer.WriteShort((short)(MessageId << 2 | typeLen)); //write id and size of size

            switch (typeLen)//write the size
            {
                case 0:
                    break;
                case 1:
                    writer.WriteByte((byte)data.Length);
                    break;
                case 2:
                    writer.WriteShort((short)data.Length);
                    break;
                case 3:
                    writer.WriteByte((byte)(data.Length >> 16 & 255));
                    writer.WriteShort((short)(data.Length & 65535));
                    break;
            }
            writer.WriteBytes(data);//write the packet after write the header
        }
    }
}
