


















// Generated on 06/26/2015 11:41:55
using System;
using System.Collections.Generic;
using System.Linq;
using RaidBot.Protocol.Types;
using RaidBot.Common.IO;

namespace RaidBot.Protocol.Messages
{

public class SetEnableAVARequestMessage : NetworkMessage
{

public const uint Id = 6443;
public override uint MessageId
{
    get { return Id; }
}

public bool enable;
        

public SetEnableAVARequestMessage()
{
}

public SetEnableAVARequestMessage(bool enable)
        {
            this.enable = enable;
        }
        

public override void Serialize(ICustomDataWriter writer)
{

writer.WriteBoolean(enable);
            

}

public override void Deserialize(ICustomDataReader reader)
{

enable = reader.ReadBoolean();
            

}


}


}