


















// Generated on 06/26/2015 11:41:53
using System;
using System.Collections.Generic;
using System.Linq;
using RaidBot.Protocol.Types;
using RaidBot.Common.IO;

namespace RaidBot.Protocol.Messages
{

public class AccessoryPreviewErrorMessage : NetworkMessage
{

public const uint Id = 6521;
public override uint MessageId
{
    get { return Id; }
}

public sbyte error;
        

public AccessoryPreviewErrorMessage()
{
}

public AccessoryPreviewErrorMessage(sbyte error)
        {
            this.error = error;
        }
        

public override void Serialize(ICustomDataWriter writer)
{

writer.WriteSByte(error);
            

}

public override void Deserialize(ICustomDataReader reader)
{

error = reader.ReadSByte();
            if (error < 0)
                throw new Exception("Forbidden value on error = " + error + ", it doesn't respect the following condition : error < 0");
            

}


}


}