


















// Generated on 06/26/2015 11:41:32
using System;
using System.Collections.Generic;
using System.Linq;
using RaidBot.Protocol.Types;
using RaidBot.Common.IO;

namespace RaidBot.Protocol.Messages
{

public class PartyRefuseInvitationMessage : AbstractPartyMessage
{

public const uint Id = 5582;
public override uint MessageId
{
    get { return Id; }
}



public PartyRefuseInvitationMessage()
{
}

public PartyRefuseInvitationMessage(uint partyId)
         : base(partyId)
        {
        }
        

public override void Serialize(ICustomDataWriter writer)
{

base.Serialize(writer);
            

}

public override void Deserialize(ICustomDataReader reader)
{

base.Deserialize(reader);
            

}


}


}