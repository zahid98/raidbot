


















// Generated on 06/26/2015 11:41:34
using System;
using System.Collections.Generic;
using System.Linq;
using RaidBot.Protocol.Types;
using RaidBot.Common.IO;

namespace RaidBot.Protocol.Messages
{

public class TreasureHuntAvailableRetryCountUpdateMessage : NetworkMessage
{

public const uint Id = 6491;
public override uint MessageId
{
    get { return Id; }
}

public sbyte questType;
        public int availableRetryCount;
        

public TreasureHuntAvailableRetryCountUpdateMessage()
{
}

public TreasureHuntAvailableRetryCountUpdateMessage(sbyte questType, int availableRetryCount)
        {
            this.questType = questType;
            this.availableRetryCount = availableRetryCount;
        }
        

public override void Serialize(ICustomDataWriter writer)
{

writer.WriteSByte(questType);
            writer.WriteInt(availableRetryCount);
            

}

public override void Deserialize(ICustomDataReader reader)
{

questType = reader.ReadSByte();
            if (questType < 0)
                throw new Exception("Forbidden value on questType = " + questType + ", it doesn't respect the following condition : questType < 0");
            availableRetryCount = reader.ReadInt();
            

}


}


}