


















// Generated on 06/26/2015 11:40:59
using System;
using System.Collections.Generic;
using System.Linq;
using RaidBot.Protocol.Types;
using RaidBot.Common.IO;

namespace RaidBot.Protocol.Messages
{

public class DebugHighlightCellsMessage : NetworkMessage
{

public const uint Id = 2001;
public override uint MessageId
{
    get { return Id; }
}

public int color;
        public ushort[] cells;
        

public DebugHighlightCellsMessage()
{
}

public DebugHighlightCellsMessage(int color, ushort[] cells)
        {
            this.color = color;
            this.cells = cells;
        }
        

public override void Serialize(ICustomDataWriter writer)
{

writer.WriteInt(color);
            writer.WriteUShort((ushort)cells.Length);
            foreach (var entry in cells)
            {
                 writer.WriteVaruhshort(entry);
            }
            

}

public override void Deserialize(ICustomDataReader reader)
{

color = reader.ReadInt();
            var limit = reader.ReadUShort();
            cells = new ushort[limit];
            for (int i = 0; i < limit; i++)
            {
                 cells[i] = reader.ReadVaruhshort();
            }
            

}


}


}