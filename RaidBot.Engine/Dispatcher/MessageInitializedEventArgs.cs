﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaidBot.Engine.Enums;
using RaidBot.Engine.Manager;
using RaidBot.Protocol.Messages;
namespace RaidBot.Engine.Dispatcher
{
    public class MessageInitializedEventArgs:EventArgs
    {
        public NetworkMessage Message { get; private set; }
        public ConnectedHost mHost;
        public MessageInitializedEventArgs(NetworkMessage message , ConnectedHost Host)
        {
            Message = message;
            mHost = Host;
        }
    }
}
