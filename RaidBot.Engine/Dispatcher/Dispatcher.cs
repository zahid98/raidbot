﻿using RaidBot.Common.Default.Loging;
using RaidBot.Common.IO;
using RaidBot.Engine.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using RaidBot.Engine.Manager;
using RaidBot.Protocol.Messages;

namespace RaidBot.Engine.Dispatcher
{
    public class Dispatcher 
    {
        private List<IMessagesHandler> MessagessHandlers;
        private ConnectedHost mHost;
        public Dispatcher(ConnectedHost Host)
        {
            MessagessHandlers = new List<IMessagesHandler>();
            mHost = Host;
        }

        public event EventHandler<MessageInitializedEventArgs> MessageInitialized;
        private void OnMessageInitialized(NetworkMessage message , ConnectedHost mHost)
        {
            if (MessageInitialized != null)
                MessageInitialized(this, new MessageInitializedEventArgs(message , mHost));
        }
       
        public bool DispatchMessage(NetworkMessage message, ConnectedHost mHost)
        {
            bool isInitialized = false;
            bool retVal = true;
           foreach(var handler in MessagessHandlers)
           {
               if (handler.ContainsType(message.GetType()))
               {
                   if(!isInitialized)
                   {
                       message.Deserialize(new CustomDataReader(message.Data));
                       isInitialized = true;
                       OnMessageInitialized(message , mHost);
                   }
                   if(retVal==true)
                   {
                       retVal = handler.DispatchMessage(message, mHost);
                   }
                   else
                   {
                       handler.DispatchMessage(message, mHost);
                   }
               }
           }
            return retVal;
        }

        public void Register(IMessagesHandler handler)
        {
            MessagessHandlers.Add(handler);
        }

        public void UnRegister(IMessagesHandler handler)
        {
            if (MessagessHandlers.Contains(handler))
                MessagessHandlers.Remove(handler);
        }
    }
}