﻿using System;
using System.Collections.Generic;
using System.Collections;
using RaidBot.Common.Processe;
using RaidBot.Common.Network.Server;
using RaidBot.Engine.Manager.MITM;
using RaidBot.Engine.Manager.FS;
using RaidBot.Engine.Setting;
using RaidBot.Engine.Manager;
using RaidBot.Common;
using System.Net;
using RaidBot.Common.Default.Loging;
using System.Net.Sockets;
using System.Diagnostics;
using RaidBot.Engine.Model.Game.Player.Spells;
using RaidBot.Engine.Frames.Game.World.Fight;
using System.IO;
using System.Reflection;
using RaidBot.Engine.Model.Game.Player.Fight;
using System.Runtime.InteropServices;
using RaidBot.Protocol.Messages;
namespace RaidBot
{
    class MainClass
    {
        public static List<ConnectionManager> Managers;
        public static ConnectionManager Manager;
        static bool AlreadyAttached = false;
        static string TrollingSentence = "Tu t fou de ma gueule mec , insère un nombre valide -.- ...";
        static string ConfigPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Config";
        public static void Main(string[] args)
        {
            Succes("Raidbot !");
            Server.Default.ConnectionAccepted += Default_ConnectionAccepted;
            ClientAutoInjector.Default.Start();
            Server.Default.Start(RemoteSetting.Default.LoginListenPort);
            Succes("AutoInjector et serveur démarrés .");
            Managers = new List<ConnectionManager>();
            CommandParse(Console.ReadLine());

        }
        public static void CommandParse(string arg)
        {
            if (arg == null)
                return;
            arg.ToLower();
            switch (arg)
            {

                case "start":
                    ClientAutoInjector.Default.Start();
                    Server.Default.Start(RemoteSetting.Default.LoginListenPort);
                    Info("AutoInjector et serveur démarrés .");
                    break;

                case "stop":
                    ClientAutoInjector.Default.Stop();
                    Error("AutoInjector et serveur arrêtés .");
                    break;

                case "dofus":
                    Info("Lancement du processus Dofus.exe .");
                    Process.Start(@"C:Users\Zakaria\AppData\Local\Ankama\Dofus2\App\Dofus.exe");
                    break;

                case "full socket start":
                    LoadSocket();
                    break;

                case "ping":
                    ((MITMManager)Manager).ping();
                    break;
                case "show cell":
                    if (!Manager.Host.isConnected)
                    {
                        Error("Ce bot n'est pas encore connecté , utilisez la commande 'switch bot' pour configurer un autre bot");
                        break;
                    }
                   Info("inserez la cellID .");
                   string input = Console.ReadLine();
                    int bot;
                    if (int.TryParse(input, out bot) && bot < 600 && bot >= 0)
                      ((MITMManager)Manager).SendMessage(new ShowCellMessage(((MITMManager)Manager).Host.Bot.Game.Player.PlayerBaseInformations.Id, (ushort)bot) , Engine.Enums.DestinationEnum.CLIENT);
                    else
                    {
                        Error(TrollingSentence);
                    }
                    break;
                case "save config":
                   SaveConfig((MITMManager)Manager);
                    break;
                case "load config":
                    LoadConfig();
                    break;

                case "switch bot":
                    SwitchBot();
                    break;
                case "config":
                    if (Manager.Host.isConnected)
                        StartConfig();
                    else
                        Error("Ce bot n'est pas encore connecté , utilisez la commande 'switch bot' pour configurer un autre bot");
                    break;
                case "top":
                    ((MITMManager)Manager).Host.Bot.Game.Player.Move(Protocol.Enums.DirectionsEnum.DIRECTION_NORTH);
                    break;
                case "bot":
                    ((MITMManager)Manager).Host.Bot.Game.Player.Move(Protocol.Enums.DirectionsEnum.DIRECTION_SOUTH);
                    break;
                case "right":
                    ((MITMManager)Manager).Host.Bot.Game.Player.Move(Protocol.Enums.DirectionsEnum.DIRECTION_EAST);
                    break;
                case "left":
                    ((MITMManager)Manager).Host.Bot.Game.Player.Move(Protocol.Enums.DirectionsEnum.DIRECTION_WEST);
                    break;
                case "fight":
                    ((MITMManager)Manager).Host.Bot.Game.World.fightingFrame.LaunchLittleFight();
                    break;
                default:
                    Error("Commande \'" + arg + "\' inconnue");
                    break;

            }
            CommandParse(Console.ReadLine());
        }
        public static void Default_ConnectionAccepted(object sender, Socket acceptedSocket)
        {
            Succes("Nouveau Client Connecté .");
            Managers.Add(new MITMManager(acceptedSocket, IPAddress.Parse(RemoteSetting.Default.LoginConnectionAdresse), RemoteSetting.Default.LoginListenPort));
            Manager = Managers[0];
            Manager.Start();
            if (!AlreadyAttached)
            {
                Manager.Host.logger.OnLog += WriteLog;
                Logger.Default.OnLog += WriteLog;
            }
            AlreadyAttached = true;
        }
        public static void WriteLog(String text, RaidBot.Common.Default.Loging.LogLevelEnum level)
        {
            switch (level)
            {
                case LogLevelEnum.CriticalError:
                    Info(text);
                    break;
                case LogLevelEnum.Error:
                    Error(text);
                    break;
                case LogLevelEnum.Info:
                    Debug(text);
                    break;
                case LogLevelEnum.Succes:
                    Succes(text);
                    break;
            }
        }
        public static void ColoredConsoleWrite(ConsoleColor color, string text)
        {
            ConsoleColor originalColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            if(Manager != null && Manager.Host.isConnected)
            Console.WriteLine('[' + Manager.Host.Bot.Game.Player.PlayerBaseInformations.Name + ']' +text);
            else
            Console.WriteLine(text);
            Console.ForegroundColor = originalColor;
        }
        public static void Debug(string text)
        {
            ColoredConsoleWrite(ConsoleColor.Blue, text);
        }
        public static void Info(string text)
        {
            ColoredConsoleWrite(ConsoleColor.White, text);
        }
        public static void Error(string text)
        {
            ColoredConsoleWrite(ConsoleColor.Red, text);
        }
        public static void Succes(string text)
        {
            ColoredConsoleWrite(ConsoleColor.Green, text);
        }
        public static void LoadSocket()
        {
            Manager = new FSManager();
            Manager.Host.logger.OnLog += WriteLog;
            Logger.Default.OnLog += WriteLog;
            Info("Veuillez saisir le ndc .");
            Manager.Host.ndc = Console.ReadLine();
            Info("Veuillez saisir le mdp .");
            Manager.Host.mdp = Console.ReadLine();
            Console.WriteLine("Connexion ne cours...");
            Manager.Start();

        }
        public static void SwitchBot()
        {
            Info("Liste de bots :");
            if (Managers.Count == 0)
            {
                Error("Aucun bot connecté ...");

            }
            for (int i = 0; i < Managers.Count; i++)
            {
                if (Managers[i].Host.isConnected)
                    Info(i + ": " + Managers[i].Host.Bot.Game.Player.PlayerBaseInformations.Name);
                else
                    Info(i + ": unknown");
            }
            Info("Choissisez nombre .");

            string input = Console.ReadLine();
            int bot;
            if (int.TryParse(input, out bot) && Managers.Count - 1 >= bot)
                Manager = Managers[bot];
            else
            {
                Error(TrollingSentence);
                SwitchBot();
            }
        }
        #region Configuration
        public static void StartConfig()
        {
            Info("Que voulez vous configurer ?");
            Info("0 : Combats");
            string input = Console.ReadLine();
            int choice;
            if (int.TryParse(input, out choice))
            {
                switch (choice)
                {
                    case 0:
                        StartFightConfig();
                        break;
                }
            }

        }
        #region Configuration combats
        public static void StartFightConfig()
        {
            Info("Que voulez vous configurer dans les combats ?");
            Info("0 : Sorts");
            string input = Console.ReadLine();
            int choice;
            if (int.TryParse(input, out choice))
            {
                switch (choice)
                {
                    case 0:
                        StartSpellsConfig();
                        break;
                }
            }
            else
            {
                Error(TrollingSentence);
                StartFightConfig();
            }

        }
        #region Configuration sorts
        public static void StartSpellsConfig()
        {
            Info("Liste des sorts");
            for (int i = 0; i < Manager.Host.Bot.Game.Player.PlayerSpells.Spells.Count; i++)
            {
                Info(i + " :" + Manager.Host.Bot.Game.Player.PlayerSpells.Spells[i].Name);
            }
            string input = Console.ReadLine();
            int choice;
            if (int.TryParse(input, out choice))
            {
                StartSpellConfig(Manager.Host.Bot.Game.Player.PlayerSpells.Spells[choice]);
            }
            else
            {
                Error(TrollingSentence);
                StartSpellsConfig();
            }
        }
        public static void StartSpellConfig(SpellData spell)
        {
            Info("Configuration du sort " + spell.Name);
            Info("Choisissez une cible :");
            Info("0 :" + CibleEnum.ENNEMY.ToString());
            Info("1 :" + CibleEnum.ALLY.ToString());
            Info("2 :" + CibleEnum.INVOCATION.ToString());
            Info("3 :" + CibleEnum.SELF.ToString());
            string input = Console.ReadLine();
            int cible;
            if (int.TryParse(input, out cible))
            {
                Info("Définissez la priorité du sort");
                string minput = Console.ReadLine();
                int priority;
                if (int.TryParse(minput, out priority))
                {
                    switch (cible)
                    {
                        case 0:
                            Manager.Host.Bot.Game.Player.PlayerFightInformations.UsableSpells.Add(new Engine.Model.Game.Player.Fight.UsableSpellModel(spell, CibleEnum.ENNEMY, priority));
                            Info("Spell added to usable spells : " + spell.Name + '-' + CibleEnum.ENNEMY.ToString() + '-' + priority);
                            return;
                        case 1:
                            Manager.Host.Bot.Game.Player.PlayerFightInformations.UsableSpells.Add(new Engine.Model.Game.Player.Fight.UsableSpellModel(spell, CibleEnum.ALLY, priority));
                            Info("Spell added to usable spells : " + spell.Name + '-' + CibleEnum.ALLY.ToString() + '-' + priority);
                            return;
                        case 2:
                            Manager.Host.Bot.Game.Player.PlayerFightInformations.UsableSpells.Add(new Engine.Model.Game.Player.Fight.UsableSpellModel(spell, CibleEnum.INVOCATION, priority));
                            Info("Spell added to usable spells : " + spell.Name + '-' + CibleEnum.INVOCATION.ToString() + '-' + priority);
                            return;
                        case 3:
                            Manager.Host.Bot.Game.Player.PlayerFightInformations.UsableSpells.Add(new Engine.Model.Game.Player.Fight.UsableSpellModel(spell, CibleEnum.SELF, priority));
                            Info("Spell added to usable spells : " + spell.Name + '-' + CibleEnum.SELF.ToString() + '-' + priority);
                            return;
                    }

                }
                else
                {
                    Error(TrollingSentence);
                    StartSpellsConfig();
                }
            }
        }
        #endregion

        #endregion

        #endregion

        #region Saving config
        private static void SaveConfig(MITMManager manager)
        {
            if (!Directory.Exists(ConfigPath + @"\" + manager.Host.Bot.Game.Player.PlayerBaseInformations.Name))
            {
                Directory.CreateDirectory(ConfigPath + @"\" + manager.Host.Bot.Game.Player.PlayerBaseInformations.Name);
            }
            SaveFightConfig(manager);
        }
        private static void SaveFightConfig(MITMManager manager)
        {
            if (!Directory.Exists(ConfigPath + @"\" + manager.Host.Bot.Game.Player.PlayerBaseInformations.Name + @"\Combats" ))
            {
                Directory.CreateDirectory(ConfigPath + @"\" + manager.Host.Bot.Game.Player.PlayerBaseInformations.Name + @"\Combats");
            }
           if (!File.Exists(ConfigPath + @"\" + manager.Host.Bot.Game.Player.PlayerBaseInformations.Name + @"\Combats\Sorts.ini"))
           {
               File.Create(ConfigPath + @"\" + manager.Host.Bot.Game.Player.PlayerBaseInformations.Name + @"\Combats\Sorts.ini").Close();
           }
           string[] Lines = File.ReadAllLines(ConfigPath + @"\" + manager.Host.Bot.Game.Player.PlayerBaseInformations.Name + @"\Combats\Sorts.ini");
          using (System.IO.StreamWriter file = new System.IO.StreamWriter(ConfigPath + @"\" + manager.Host.Bot.Game.Player.PlayerBaseInformations.Name + @"\Combats\Sorts.ini"))
          {
            foreach(UsableSpellModel entry in manager.Host.Bot.Game.Player.PlayerFightInformations.UsableSpells)
            {
                string configToWrite = entry.Spell.Name + '-' + entry.Cible.ToString() + '-' + entry.Priority.ToString();
                if (AlreadyWriten(configToWrite, Lines))
                    continue;
                else
                    file.WriteLine(configToWrite);
            }
        }
        }
        private static bool AlreadyWriten(string ine , string[] Lines)
        {
            foreach (string line in Lines)
            {
                if (line == ine)
                    return true;
            }
            return false;
        }
       
        #endregion

        #region Loading Config
        private static void LoadConfig()
        {
            if (!Directory.Exists(ConfigPath + @"\" + Manager.Host.Bot.Game.Player.PlayerBaseInformations.Name))
            {
                Error("Aucune configuration n'est sauvegardée pour ce joueur");
                return;
            }
            LoadFightConfig();
        }
        #region Loading fighting config
        public static void LoadFightConfig()
        {
            if (!Directory.Exists(ConfigPath + @"\" + Manager.Host.Bot.Game.Player.PlayerBaseInformations.Name + @"\Combats"))
            {
                Error("Aucune configuration de combats n'est sauvegardée pour ce joueur");
                return;
            }
            if (!File.Exists(ConfigPath + @"\" + Manager.Host.Bot.Game.Player.PlayerBaseInformations.Name + @"\Combats\Sorts.ini"))
            {
                Error("Aucune configuration de sorts n'est sauvegardée pour ce joueur");
                return;
            }
            string[] Lines = File.ReadAllLines(ConfigPath + @"\" + Manager.Host.Bot.Game.Player.PlayerBaseInformations.Name + @"\Combats\Sorts.ini");
           foreach (string line in Lines)
           {
            string[] parameters =   line.Split('-');
             if(Manager.Host.Bot.Game.Player.PlayerFightInformations.UsableSpells.Exists(spell => spell.Spell.Name == parameters[0]))
                 continue;
               Manager.Host.Bot.Game.Player.PlayerFightInformations.UsableSpells.Add(new UsableSpellModel( Manager.Host.Bot.Game.Player.PlayerSpells.Spells.Find(spell => spell.Name == parameters[0]), (CibleEnum)Enum.Parse(typeof(CibleEnum) , parameters[1]) , int.Parse(parameters[2])));
           }
        }
        
        #endregion
        #endregion

    }
}
